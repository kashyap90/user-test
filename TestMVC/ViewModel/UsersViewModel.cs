﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestMVC.Models;

namespace TestMVC.ViewModel
{
    public class UsersViewModel
    {
        public UsersViewModel()
        {

        }
        public UsersViewModel(List<User> user)
        {
            UsersView = user;
        }
        //public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        //public string FormCollection { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<User> UsersView { get; set; }
        public List<int> userID { get; set; }
    }
}