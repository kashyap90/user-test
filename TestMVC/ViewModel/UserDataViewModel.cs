﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestMVC.Models;

namespace TestMVC.ViewModel
{
    public class UserDataViewModel
    {
        public UserDataViewModel()
        {

        }
        public UserDataViewModel(List<UserData> users)
        {
            UserDatas = users;
        }
        public IEnumerable<UserData> UserDatas { get; set; }
    }
}