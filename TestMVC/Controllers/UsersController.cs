﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestMVC.Models;
using TestMVC.ViewModel;

namespace TestMVC.Controllers
{
    public class UsersController : Controller
    {
        private Test_MVCEntities _context;
        public UsersController()
        {
            _context = new Test_MVCEntities();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Users
        public ActionResult Index()
        {
            //var users = _context.Users.Where(m=>m.IsActive == false).OrderBy(c => c.ID).ToList();
            var users = new List<User>();
            UsersViewModel usersViewModel = new UsersViewModel();
            usersViewModel.Name = "";
            //usersViewModel.UsersView = users;
            if (TempData["Error"] != null)
            {
                ViewBag.Message = TempData["Error"].ToString();
                if (TempData["Error"].ToString() == "Active")
                {
                    users = _context.Users.Where(m => m.IsActive == true).OrderBy(c => c.ID).ToList();
                    usersViewModel.UsersView = users;
                }
                else
                {
                    users = _context.Users.Where(m => m.IsActive == false).OrderBy(c => c.ID).ToList();
                    usersViewModel.UsersView = users;
                }
            }
            else
            {
                users = _context.Users.Where(m => m.IsActive == false).OrderBy(c => c.ID).ToList();
                usersViewModel.UsersView = users;
            }
            
            return View(usersViewModel);
        }
        
        public ActionResult Update(UsersViewModel usersViewModel, string Save,string Active, string InActive, string Update, List<string> checkUser, FormCollection collection)
        {
            if (!string.IsNullOrEmpty(Save))
            {
                if (!ModelState.IsValid)
                {
                    TempData["Error"] = "please provide name";
                    return RedirectToAction("Index");
                }
                User user = new User();
                user.ID = 0;
                user.Name = usersViewModel.Name;
                user.IsActive = false;
                _context.Users.Add(user);
                _context.SaveChanges();
                TempData["Error"] = "Detail Saved";
                return RedirectToAction("Index");
            }
            if (!string.IsNullOrEmpty(Active))
            {
                TempData["Error"] = "Active";
                return RedirectToAction("Index");
            }
            if (!string.IsNullOrEmpty(InActive))
            {
                TempData["Error"] = "InActive";
                return RedirectToAction("Index");
            }
            if (!string.IsNullOrEmpty(Update))
            {
                if (string.IsNullOrEmpty(Request.Form["AllUser"]))
                {
                    TempData["Error"] = "No User Available";
                    return RedirectToAction("Index");
                }
                string strSelected = "0";
                if (!string.IsNullOrEmpty(Request.Form["checkUser"]))
                {
                    //TempData["Error"] = "Please Select Users for Activation";
                    //return RedirectToAction("Index");
                    strSelected = Request.Form["checkUser"];
                }
               
                string strAll = Request.Form["AllUser"];

                string[] arrChecked = strSelected.Split(',');
                string[] arrUserAll = strAll.Split(',');
                ArrayList arrListUnChecked = new ArrayList();
                ArrayList arrListChecked = new ArrayList();

                for (int i = 0; i < arrUserAll.Count(); i++)
                {
                    arrListUnChecked.Add(Convert.ToInt32(arrUserAll[i]));
                }
                //if (!string.IsNullOrEmpty(strSelected))
                //{

                //}
                for (int i = 0; i < arrChecked.Count(); i++)
                {
                    arrListChecked.Add(Convert.ToInt32(arrChecked[i]));
                    arrListUnChecked.Remove(Convert.ToInt32(arrChecked[i]));
                }
                arrListChecked.Remove(0);
                foreach (int id in arrListChecked)
                {
                    var userInDb = _context.Users.Where(m => m.ID == id).SingleOrDefault();
                    if (userInDb.IsActive != true)
                    {
                        userInDb.IsActive = true;
                    }
                }

                foreach (int id in arrListUnChecked)
                {
                    var userInDb = _context.Users.Where(m => m.ID == id).SingleOrDefault();
                    if (userInDb.IsActive != false)
                    {
                        userInDb.IsActive = false;
                    }
                }

                _context.SaveChanges();
                TempData["Error"] = "Details Updated";
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        public ActionResult UserData()
        {
            var userData = _context.UserDatas.ToList();
            //UserDataViewModel userDataViewModel = new UserDataViewModel(userData);
            return View("UserData", userData);
        }

        public ActionResult UpdateUserData(IEnumerable<UserData> userData)
        {
            if (userData != null)
            {
                foreach (var user in userData)
                {
                    int id = user.ID;
                    string name = user.Name;
                }
            }
            return RedirectToAction("UserData");
        }
    }
}