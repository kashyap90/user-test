﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestMVC.Models;

namespace TestMVC.Controllers
{
    public class UserEditController : Controller
    {
        private Test_MVCEntities _context;
        public UserEditController()
        {
            _context = new Test_MVCEntities();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: UserEdit
        public ActionResult Index()
        {
            var users = _context.Users.OrderBy(c => c.ID).ToList();
            return View("Edit",users);
        }

        public ActionResult Edit(List<User> userList)
        {
            foreach (var user in userList)
            {
                var userInDb = _context.Users.Where(m => m.Name == user.Name).SingleOrDefault();
                if (userInDb.IsActive != user.IsActive)
                {
                    userInDb.IsActive = user.IsActive;
                }
            }

            _context.SaveChanges();
            TempData["Error"] = "Details Updated";
            return RedirectToAction("Index");
        }
    }
}