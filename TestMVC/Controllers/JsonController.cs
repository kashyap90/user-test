﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestMVC.Models;

namespace TestMVC.Controllers
{
    public class JsonController : Controller
    {
        private Test_MVCEntities _context;
        public JsonController()
        {
            _context = new Test_MVCEntities();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Json
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AllUsers(string id) 
        {
            var listUser = _context.Users.Where(n => n.Name.Contains(id)).ToList();
            return Json(listUser, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AllUsersSecond(string id)
        {
            string query = id.Split(',')[1];
            var listUser = _context.Users.Where(n => n.Name.Contains(query)).ToList();
            return Json(listUser, JsonRequestBehavior.AllowGet);
        }
    }
}